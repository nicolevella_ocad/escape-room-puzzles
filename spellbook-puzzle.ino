/*

  Project:      Escape Room Puzzle #1 - Spellbook
  Student:      Nicole Vella, Weiqi Wu, Darini Kapoor, Allan Wang, Kyle Shoesmith
  Course:       DGIF 2002 - Physical Computing - OCAD University
  Created on:   December 3, 2019
  Based on:     "How to Set up 7-Segment Displays on the Arduino"; Krishna Pattabiraman; http://www.circuitbasics.com/arduino-7-segment-display-tutorial/

*/

#include <Servo.h> // include the serve library so we can control the servo motor
#include "SevSeg.h" // include lib for seven segment LED display

SevSeg sevseg; // init a display called "sevseg"
Servo myservo; // init a servo called "myservo"

const int servo = 26;  // servo on digital pin 6

// pins for toggle switches
int switch8 = 38;
int switch7 = 40;
int switch6 = 42;
int switch5 = 44;
int switch4 = 46;
int switch3 = 48;
int switch2 = 50;
int switch1 = 52;

String binaryCode = String();

void setup() {

  myservo.attach(servo); // declare servo
  myservo.write(0); // set servo in lock position

  // set toggle switches as inputs
  pinMode(switch1, INPUT);
  pinMode(switch2, INPUT);
  pinMode(switch3, INPUT);
  pinMode(switch4, INPUT);
  pinMode(switch5, INPUT);
  pinMode(switch6, INPUT);
  pinMode(switch7, INPUT);
  pinMode(switch8, INPUT);

  // setup seven segment display
  byte numDigits = 4; // # of digits on display
  byte digitPins[] = {10, 11, 12, 13}; // pins connected to each digit
  byte segmentPins[] = {9, 2, 3, 5, 6, 8, 7, 4}; // pins connected to each segment
  bool resistorsOnSegments = true; // are we using a resistor?
  bool updateWithDelaysIn = true; // use a delay to update the digits?
  byte hardwareConfig = COMMON_CATHODE; // is display common cathode or common anode
  // declare the display
  sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments);
  // set brightness on display
  sevseg.setBrightness(90);

}

void loop() {

  // capture the status of each toggle and append them to a string which represents a binary numnber of all the switches
  binaryCode = "1"; // clue for player to indicate switches represent binary data, this is written on the book cover
  binaryCode += String(digitalRead(switch1));
  binaryCode += String(digitalRead(switch2));
  binaryCode += String(digitalRead(switch3));
  binaryCode += String(digitalRead(switch4));
  binaryCode += String(digitalRead(switch5));
  binaryCode += String(digitalRead(switch6));
  binaryCode += String(digitalRead(switch7));
  binaryCode += String(digitalRead(switch8));
  binaryCode += "0"; // clue for player to indicate switches represent binary data, this is written on the book cover

  // convert the string to a number using the string-to-unsigned-long function in C, takes 3 arguments (string to convert, ???, base-type of number to convert FROM, base2 is binary)
  unsigned long disp = strtoul(binaryCode.c_str(), NULL, 2);

  // update the number on the display with the converted string
  sevseg.setNumber(disp);
  sevseg.refreshDisplay();

  // check if the user won
  didUserWin();

}


// function that checks if the user input the buttons in the right order
void didUserWin() {

  // if the convert string is 666, the user wins. unlock the book;
  if (disp == 666) {

    myservo.write(90);

  } else {

    myservo.write(0);

  }
}